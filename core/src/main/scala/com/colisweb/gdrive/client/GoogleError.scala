package com.colisweb.gdrive.client

import com.colisweb.gdrive.client.sheets.GoogleSpreadsheetId
import eu.timepit.refined.types.numeric.NonNegInt

sealed trait GoogleError extends Exception

final case class FolderNotFound(keywords: String) extends GoogleError

final case class SpreadsheetNotFound(keywords: String) extends GoogleError

final case class CsvFileNotFound(keywords: String) extends GoogleError

final case class FileNotFound(keywords: String) extends GoogleError

final case class DataSourceIdNotFound(keywords: String) extends GoogleError

case class SpreadsheetError(keywords: String) extends Exception(keywords) with GoogleError

object EmptySpreadsheet extends SpreadsheetError("The spreadsheet is empty")
object NoColumnInSpreadsheet {
  def apply(spreadsheetId: GoogleSpreadsheetId, column: String, row: NonNegInt): SpreadsheetError = {
    SpreadsheetError(s"No column `$column` in spreadsheet `$spreadsheetId` at row $row")
  }
}
