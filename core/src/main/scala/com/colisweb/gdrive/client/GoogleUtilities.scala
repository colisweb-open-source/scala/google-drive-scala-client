package com.colisweb.gdrive.client

//import com.colisweb.gdrive.client.GoogleUtilities.NonEmptyCell
import com.google.api.services.sheets.v4.model._
import eu.timepit.refined.api.Refined
import eu.timepit.refined.numeric.NonNegative
import eu.timepit.refined.types.numeric.NonNegInt

import scala.jdk.CollectionConverters._

object GoogleUtilities {

  def asRowData(data: List[String], format: CellFormat): RowData =
    new RowData().setValues(
      data.map { d =>
        new CellData()
          .setUserEnteredValue(new ExtendedValue().setStringValue(d))
          .setEffectiveFormat(format)
      }.asJava
    )

  implicit class GoogleListData[A](data: java.util.List[A]) {
    def asScalaListNotNull: List[A] = data match {
      case null => Nil
      case _    => data.asScala.toList
    }
  }

  def getRowDataHeaders(line: RowData): Map[String, NonNegInt] =
    line.getValues.asScalaListNotNull
      .map(_.getFormattedValue)
      .zipWithIndex
      .map { case (k, v) => k -> Refined.unsafeApply[Int, NonNegative](v) }
      .toMap

  private[client] def getRowValuesOption(row: RowData, headers: List[String]): Map[String, Option[String]] = {
    val maybeRow = for {
      _     <- Option(row)
      cells <- Option(row.getValues)
    } yield headers.zip(cells.asScala).collect { case (header, cell) => header -> NonEmptyCell(cell) }.toMap
    maybeRow.getOrElse(Map.empty)
  }

  def getRowValues(row: RowData, headers: List[String]): Map[String, String] = {
    getRowValuesOption(row, headers).collect { case (header, Some(cell)) => header -> cell }
  }

  def convertColumnIndexToLetter(index: Int): String = {
    val base = 65

    if (index > 25) {
      val quotient = index / 26 - 1
      val modulo   = index % 26

      s"${(base + quotient).toChar}${(base + modulo).toChar}"
    } else
      (base + index).toChar.toString
  }

  def formatAsHyperlink(hyperlink: String): String =
    s"""=HYPERLINK("$hyperlink")"""

  private case object NonEmptyCell {
    def apply(cell: CellData): Option[String] = {
      cell match {
        case _ if cell.isEmpty                   => None
        case _ if cell.getFormattedValue == null => None
        case _ if cell.getFormattedValue.isBlank => None
        case _                                   => Some(cell.getFormattedValue)
      }
    }
  }
}
