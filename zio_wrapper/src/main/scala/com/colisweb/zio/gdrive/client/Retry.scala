package com.colisweb.zio.gdrive.client

import zio.{Schedule, Task, ZIO}

class Retry(retryPolicy: Schedule[Any, Throwable, Any]) {
  def apply[A](computation: => A): Task[A] =
    ZIO
      .attempt(computation)
      .tapError(e => ZIO.logWarning(s"error occurred in google-drive-scala : ${e.toString}"))
      .retry(retryPolicy)
}
