package com.colisweb.zio.gdrive.client

import zio.{Clock, IO, Unsafe}

trait ZIOHelpers {

  implicit class UnsafeRunImplicitWithBlockingLoggingAndClock[A, E1](computation: IO[E1, A]) {

    def unsafeRun(clock: Clock = Clock.ClockLive): A =
      Unsafe.unsafe { implicit unsafe =>
        zio.Runtime.default.unsafe
          .run(computation.withClock(clock).provide(ZIOLiveLogging.live("test")))
          .getOrThrowFiberFailure()
      }
  }
}

object ZIOHelpers extends ZIOHelpers
