package com.colisweb.gdrive.client.sheets

sealed trait GoogleSheetsMode

object GoogleSheetsMode {

  case object Local        extends GoogleSheetsMode
  case object GoogleSheets extends GoogleSheetsMode

}
