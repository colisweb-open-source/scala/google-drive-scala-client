package com.colisweb.gdrive.client.sheets

import eu.timepit.refined.types.numeric.NonNegInt

final case class GoogleSpreadsheet(
    rows: List[SpreadsheetRow]
) {
  override def toString: String = {
    val rowsString = rows.map(r => s"${r.index}:${r.cells.mkString("|")}")
    "Spreadsheet(" + rowsString.mkString(";") + ")"
  }

}

final case class SpreadsheetRow(
    index: NonNegInt,
    cells: Map[String, String]
) {

  def isCellBlank(columnName: String): Boolean =
    cells.get(columnName).forall(value => isBlank(value))

  def getCellOption(columnName: String): Option[String] =
    cells.get(columnName)

  def getCellOptionNonBlank(columnName: String): Option[String] =
    cells.get(columnName).filterNot(isBlank)

  private def isBlank(value: String) = value.isEmpty || value.isBlank
}
