package com.colisweb.zio.gdrive.client

import com.colisweb.gdrive.client.{GoogleAuthenticator, SpreadsheetError}
import com.colisweb.gdrive.client.drive.GoogleDriveClient
import com.colisweb.gdrive.client.sheets.{
  GoogleGridProperties,
  GoogleSheetProperties,
  GoogleSpreadsheetId,
  SheetRangeContent
}
import com.colisweb.zio.gdrive.client.ZIOHelpers.UnsafeRunImplicitWithBlockingLoggingAndClock
import eu.timepit.refined.api.Refined
import eu.timepit.refined.auto._
import org.scalatest.matchers.should.Matchers
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, EitherValues}
import org.scalatest.wordspec.AnyWordSpec
import eu.timepit.refined.types.numeric.NonNegInt

class GoogleSheetClientRetrySpec
    extends AnyWordSpec
    with Matchers
    with BeforeAndAfterEach
    with BeforeAndAfterAll
    with EitherValues {

  val authenticator: GoogleAuthenticator =
    GoogleAuthenticator.fromResource("google-credentials.json", Some("RoutingAnalysis"))
  val drive       = new GoogleDriveClient(authenticator)
  val clientRetry = new GoogleSheetClientRetry(authenticator)
  val headersList: List[(String, NonNegInt)] = List(
    ("column1", 0),
    ("column2", 1),
    ("column3", 2),
    ("column4", 3)
  )
  val headers: Map[String, NonNegInt] = headersList.toMap
  val columnsToCheck: List[String]    = List("column1")
  private val sheetProperties =
    List(GoogleSheetProperties("toto", GoogleGridProperties(frozenRowCount = 1)))
  var spreadsheetId: String = _

  override protected def beforeEach(): Unit = {
    spreadsheetId = clientRetry.createSpreadsheet("spreadsheet_name", sheetProperties).unsafeRun()
    super.beforeEach()
  }

  override protected def afterEach(): Unit = {
    drive.delete(spreadsheetId)
    super.afterEach()
  }

  "#getFirstFreeRowIndex" should {
    "succeed" in {
      val data = List.tabulate(2, 4)((r, c) => s"data $r $c")
      clientRetry
        .writeRanges(
          spreadsheetId,
          List(
            SheetRangeContent("A1", List(List("column1", "column2", "column3", "column4"))),
            SheetRangeContent("A2", data)
          )
        )
        .unsafeRun()
      val firstFreeRowIndex = clientRetry
        .getFirstFreeRowIndex(GoogleSpreadsheetId(Refined.unsafeApply(spreadsheetId)), headers, columnsToCheck)
        .unsafeRun()

      firstFreeRowIndex.value shouldEqual 4
    }
    "fail for a non existing column" in {
      val res = clientRetry
        .getFirstFreeRowIndex(GoogleSpreadsheetId(Refined.unsafeApply(spreadsheetId)), headers, List("randomColumn"))
        .either
        .unsafeRun()

      res.left.value shouldBe a[SpreadsheetError]
    }
  }

  "#getHeaders" should {
    "succeed if the spreadsheet has at least 1 row" in {
      clientRetry
        .writeRange(spreadsheetId, SheetRangeContent("A1", List(List("column1", "column2", "column3", "column4"))))
        .unsafeRun()

      val res = clientRetry.getHeaders(GoogleSpreadsheetId(Refined.unsafeApply(spreadsheetId))).unsafeRun()

      res shouldEqual headers
    }

    "fail if the spreadsheet is empty" in {
      val res = clientRetry.getHeaders(GoogleSpreadsheetId(Refined.unsafeApply(spreadsheetId))).either.unsafeRun()

      res.left.value shouldBe a[SpreadsheetError]
    }
  }

  "#getColumnIndex" should {
    "succeed if the column exist" in {
      val res = clientRetry
        .getColumnIndex("column1")(GoogleSpreadsheetId(Refined.unsafeApply(spreadsheetId)), headers)
        .unsafeRun()

      res.value shouldEqual 0
    }

    "fail if the column does not exist" in {
      val res = clientRetry
        .getColumnIndex("randomColumn")(GoogleSpreadsheetId(Refined.unsafeApply(spreadsheetId)), headers)
        .either
        .unsafeRun()

      res.left.value shouldBe a[SpreadsheetError]
    }
  }

  "build range cell" should {
    "build correctly" in {
      val rangeContent: SheetRangeContent = SheetRangeContent("A1", List(List("content")))

      val res = clientRetry
        .buildRangeCell("column1", "content")(
          GoogleSpreadsheetId(Refined.unsafeApply(spreadsheetId)),
          headers,
          1
        )
        .unsafeRun()

      res shouldEqual rangeContent
    }
  }

  "#extendSheetIfNeeded" should {
    "extend sheet when needed" in {
      val rowCountBeforeExtension = // to rename
        clientRetry.retrieveSheetsProperties(spreadsheetId).unsafeRun().head.getGridProperties.getRowCount
      val firstFreeRowIndex = rowCountBeforeExtension - 1
      clientRetry
        .extendSheetIfNeeded(GoogleSpreadsheetId(Refined.unsafeApply(spreadsheetId)), firstFreeRowIndex, 10, 100)
        .unsafeRun()
      val rowCountAfterExtension =
        clientRetry.retrieveSheetsProperties(spreadsheetId).unsafeRun().head.getGridProperties.getRowCount

      rowCountAfterExtension - rowCountBeforeExtension shouldEqual 100
    }

    "not extend sheet when enough rows are remaining" in {
      val rowCountBeforeExtension =
        clientRetry.retrieveSheetsProperties(spreadsheetId).unsafeRun().head.getGridProperties.getRowCount
      clientRetry.extendSheetIfNeeded(GoogleSpreadsheetId(Refined.unsafeApply(spreadsheetId)), 0, 10, 100).unsafeRun()
      val rowCountAfterExtension =
        clientRetry.retrieveSheetsProperties(spreadsheetId).unsafeRun().head.getGridProperties.getRowCount

      rowCountAfterExtension shouldEqual rowCountBeforeExtension
    }
  }
}
