package com.colisweb.zio.gdrive.client

import zio.logging.backend.SLF4J.{causeToThrowableDefault, getLoggerName, logFormatDefault, slf4j}
import zio.{Layer, Trace}

object ZIOLiveLogging {

  def live[E](loggerName: String): Layer[E, Unit] = zio.Runtime.removeDefaultLoggers >>> slf4j(
    logFormatDefault,
    { (trace: Trace) =>
      val foundName = getLoggerName("")(trace)
      s"zlogger.$loggerName.$foundName"
    },
    causeToThrowableDefault
  )

}
