package com.colisweb.gdrive.client

import com.colisweb.gdrive.client.GoogleUtilities.{
  convertColumnIndexToLetter,
  formatAsHyperlink,
  getRowDataHeaders,
  getRowValues,
  getRowValuesOption
}
import com.colisweb.gdrive.client.drive.GoogleDriveClient
import com.colisweb.gdrive.client.sheets.{
  GoogleGridProperties,
  GoogleSheetClient,
  GoogleSheetProperties,
  SheetRangeContent
}
import eu.timepit.refined.api.Refined
import org.scalatest.BeforeAndAfterAll
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class GoogleUtilitiesSpec extends AnyWordSpec with Matchers with BeforeAndAfterAll {
  val authenticator: GoogleAuthenticator =
    GoogleAuthenticator.fromResource("google-credentials.json", Some("RoutingAnalysis"))
  val drive                 = new GoogleDriveClient(authenticator)
  val client                = new GoogleSheetClient(authenticator)
  var spreadsheetId: String = _
  private val sheetProperties =
    List(GoogleSheetProperties("toto", GoogleGridProperties(frozenRowCount = 1)))

  override protected def beforeAll(): Unit = {
    super.beforeAll()
    spreadsheetId = client.createSpreadsheet("spreadsheet_name", sheetProperties)
    client.writeRanges(
      spreadsheetId,
      List(SheetRangeContent("A1", List(List("column1", "column2", "column3", "column4", "column5"))))
    )
    ()
  }

  override protected def afterAll(): Unit = {
    drive.delete(spreadsheetId)
    super.afterAll()
  }

  "getting headers from a row data" should {
    "get the headers" in {
      val row     = client.readRows(spreadsheetId, "1:1").head
      val headers = getRowDataHeaders(row)

      headers should contain theSameElementsAs List(
        ("column1", Refined.unsafeApply(0)),
        ("column2", Refined.unsafeApply(1)),
        ("column3", Refined.unsafeApply(2)),
        ("column4", Refined.unsafeApply(3)),
        ("column5", Refined.unsafeApply(4))
      ).toMap
    }
  }

  "getting row values" should {
    "get" in {
      client.writeRanges(
        spreadsheetId,
        List(SheetRangeContent("A2", List(List("value1", "value2", "value3", "value4"))))
      )

      val row = client.readRows(spreadsheetId, "2:2").head
      val res = getRowValues(row, headers = List("column1", "column2", "column3", "column4"))
      res should contain theSameElementsAs List(
        ("column1", "value1"),
        ("column2", "value2"),
        ("column3", "value3"),
        ("column4", "value4")
      ).toMap
    }

    "get with null values" in {
      client.writeRanges(
        spreadsheetId,
        List(SheetRangeContent("A3", List(List("value1", null))))
      )
      client.writeRanges(
        spreadsheetId,
        List(SheetRangeContent("D3", List(List("value4"))))
      )

      val row = client.readRows(spreadsheetId, "3:3").head
      val res = getRowValuesOption(row, headers = List("column1", "column2", "column3", "column4"))
      res should contain theSameElementsAs List(
        ("column1", Some("value1")),
        ("column2", None),
        ("column3", None),
        ("column4", Some("value4"))
      ).toMap
    }
    "get with blank cells" in {
      client.writeRanges(
        spreadsheetId,
        List(SheetRangeContent("A4", List(List("", " ", "  ", "\n", "null"))))
      )

      val row = client.readRows(spreadsheetId, "4:4").head
      val res = getRowValuesOption(row, headers = List("column1", "column2", "column3", "column4", "column5"))
      res should contain theSameElementsAs List(
        ("column1", None),
        ("column2", None),
        ("column3", None),
        ("column4", None),
        ("column5", Some("null"))
      ).toMap
    }
  }

  "converting the column index into a letter" should {
    "convert correctly" in {
      val columnLetter  = convertColumnIndexToLetter(0)
      val columnLetter2 = convertColumnIndexToLetter(27)

      columnLetter shouldEqual "A"
      columnLetter2 shouldEqual "AB"
    }
  }

  "formatting a string as hyperlink" should {
    "format correctly" in {
      val res = formatAsHyperlink("random-website.com")

      res shouldEqual "=HYPERLINK(\"random-website.com\")"
    }
  }
}
