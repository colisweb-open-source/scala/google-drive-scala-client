import CompileFlags.*
import Dependencies.*
import DependenciesScopesHandler.*
import PublishSettings.noPublishSettings

lazy val scala213               = "2.13.16"
lazy val supportedScalaVersions = List(scala213)

ThisBuild / organization       := "com.colisweb"
ThisBuild / scalaVersion       := scala213
ThisBuild / scalafmtOnCompile  := true
ThisBuild / scalafmtCheck      := true
ThisBuild / scalafmtSbtCheck   := true
ThisBuild / Test / fork        := true
ThisBuild / crossScalaVersions := supportedScalaVersions
ThisBuild / scalacOptions ++= crossScalacOptions(scalaVersion.value)

Global / onChangedBuildSource := ReloadOnSourceChanges

ThisBuild / pushRemoteCacheTo := Some(
  MavenCache("local-cache", baseDirectory.value / sys.env.getOrElse("CACHE_PATH", "sbt-cache"))
)

//// Main projects

lazy val root = Project(id = "google-drive-scala", base = file("."))
  .aggregate(core, cats_wrapper, zio_wrapper)
  .settings(noPublishSettings)

lazy val core = Project(id = "google-drive-scala-client", base = file("core"))
  .settings(
    libraryDependencies ++= compileDependencies(
      circe,
      circeGenericExtras,
      circeParser,
      googleBigQuery,
      googleClient,
      googleDrive,
      googleSheets,
      refined,
      scalaCompat,
      scalaReflect
    )
  )
  .settings(libraryDependencies ++= testDependencies(approvals, scalaTest))
  .settings(unusedCompileDependenciesFilter -= moduleFilter("org.scala-lang.modules", "scala-collection-compat"))

lazy val cats_wrapper = Project(id = "google-drive-scala-client-cats", base = file("cats_wrapper"))
  .dependsOn(core)
  .settings(libraryDependencies ++= compileDependencies(catsEffect, catsRetry))

lazy val zio_wrapper = Project(id = "google-drive-scala-client-zio", base = file("zio_wrapper"))
  .dependsOn(core)
  .settings(libraryDependencies ++= compileDependencies(logbackEncoder, zio, zioLogging))
  .settings(libraryDependencies ++= testDependencies(scalaTest))
