package com.colisweb.zio.gdrive.client

import zio.{Duration, Schedule}

import java.util.concurrent.TimeUnit

object RetryPolicies {

  def exponentialAttempts(baseDelaySec: Long = 30): Schedule[Any, Throwable, Any] =
    Schedule.exponential(Duration(baseDelaySec, TimeUnit.SECONDS)) && Schedule.recurs(5)

  def maxAttempts(maxRetry: Int = 5): Schedule[Any, Throwable, Any] = Schedule.recurs(maxRetry)

  val default: Schedule[Any, Throwable, Any] = exponentialAttempts() *> maxAttempts()
}
