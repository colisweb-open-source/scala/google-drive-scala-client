package com.colisweb.gdrive.client.sheets

import eu.timepit.refined.types.string.NonEmptyString

final case class GoogleSpreadsheetId(value: NonEmptyString)
