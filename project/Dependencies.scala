import sbt._

object Versions {
  final val approvals          = "1.3.1"
  final val catsEffect         = "3.5.4"
  final val catsRetry          = "3.1.3"
  final val circe              = "0.14.10"
  final val circeGenericExtras = "0.14.4"
  final val googleBigQuery     = "2.48.1"
  final val googleClient       = "2.7.2"
  final val googleDrive        = "v3-rev20250216-2.0.0"
  final val googleSheets       = "v4-rev20250211-2.0.0"
  final val logbackEncoder     = "8.0"
  final val refined            = "0.11.3"
  final val scalaCompat        = "2.13.0"
  final val scalaReflect       = "2.13.16"
  final val scalaTest          = "3.2.19"
  final val zio                = "2.1.16"
  final val zioLogging         = "2.5.0"
}

object Dependencies {
  final val approvals          = "com.colisweb"           %% "approvals-scala"            % Versions.approvals
  final val catsEffect         = "org.typelevel"          %% "cats-effect"                % Versions.catsEffect
  final val catsRetry          = "com.github.cb372"       %% "cats-retry"                 % Versions.catsRetry
  final val circe              = "io.circe"               %% "circe-core"                 % Versions.circe
  final val circeGenericExtras = "io.circe"               %% "circe-generic-extras"       % Versions.circeGenericExtras
  final val circeParser        = "io.circe"               %% "circe-parser"               % Versions.circe
  final val googleBigQuery     = "com.google.cloud"        % "google-cloud-bigquery"      % Versions.googleBigQuery
  final val googleClient       = "com.google.api-client"   % "google-api-client"          % Versions.googleClient
  final val googleDrive        = "com.google.apis"         % "google-api-services-drive"  % Versions.googleDrive
  final val googleSheets       = "com.google.apis"         % "google-api-services-sheets" % Versions.googleSheets
  final val logbackEncoder     = "net.logstash.logback"    % "logstash-logback-encoder"   % Versions.logbackEncoder
  final val refined            = "eu.timepit"             %% "refined"                    % Versions.refined
  final val scalaCompat        = "org.scala-lang.modules" %% "scala-collection-compat"    % Versions.scalaCompat
  final val scalaReflect       = "org.scala-lang"          % "scala-reflect"              % Versions.scalaReflect
  final val scalaTest          = "org.scalatest"          %% "scalatest"                  % Versions.scalaTest
  final val zio                = "dev.zio"                %% "zio"                        % Versions.zio
  final val zioLogging         = "dev.zio"                %% "zio-logging-slf4j"          % Versions.zioLogging
}
