package com.colisweb.zio.gdrive.client

import com.colisweb.gdrive.client.bigquery.BigQueryTable
import com.colisweb.gdrive.client.bigquery.BigQueryTable._
import com.google.auth.oauth2.GoogleCredentials
import com.google.cloud.bigquery.{Option => _, _}
import io.circe.Encoder
import io.circe.syntax._
import zio.{Schedule, Task, ZIO}

import java.nio.ByteBuffer
import java.nio.charset.StandardCharsets
import java.util.UUID
import scala.util.Try

class BigQueryTableRetry[T](
    credentials: GoogleCredentials,
    val projectId: String,
    val datasetName: String,
    val tableName: String,
    schema: Schema,
    retryPolicy: Schedule[Any, Throwable, Any] = RetryPolicies.default
)(implicit encoder: Encoder[T]) {

  private val bigQueryTable = new BigQueryTable(credentials, projectId, datasetName, tableName, schema)(encoder)
  private val retry         = new Retry(retryPolicy)

  lazy val storedTable: Task[Option[Table]]   = retry(bigQueryTable.storedTable)
  lazy val storedSchema: Task[Option[Schema]] = retry(bigQueryTable.storedSchema)

  def appendRows(data: Iterable[T], allowSchemaUpdate: Boolean): Task[Unit] =
    (maybeUpdateSchema().when(allowSchemaUpdate)
      *> uploadData(data))

  def updateRows(
      data: Iterable[T],
      fieldsToUpdate: Map[String, T => String],
      conditions: Iterable[WhereCondition]
  ): Task[Unit] =
    ZIO.foreachDiscard(data)(row => retry(bigQueryTable.updateRow(fieldsToUpdate, conditions)(row)))

  def deleteRows(conditions: Iterable[WhereCondition]): Task[Unit] =
    retry(bigQueryTable.deleteRows(conditions)).unit

  def executeQuery(query: String, jobPrefix: String): Task[TableResult] =
    retry(bigQueryTable.executeQuery(query, jobPrefix))

  def maybeUpdateSchema(): Task[Unit] =
    retry(bigQueryTable.maybeUpdateSchema())

  def getAllRows: Task[Iterable[FieldValueList]] =
    retry(bigQueryTable.getAllRows)

  private def waitForJob(jobId: JobId): Task[Try[Job]] =
    retry(bigQueryTable.waitForJob(jobId))

  def uploadData(data: Iterable[T]): Task[Unit] = {
    val writeJobConfig =
      WriteChannelConfiguration
        .newBuilder(TableId.of(datasetName, tableName))
        .setFormatOptions(FormatOptions.json())
        .setSchema(schema)
        .build()

    val jobId = JobId.newBuilder().setJob(s"appendJob_${UUID.randomUUID.toString}").build()

    ZIO.scoped {
      ZIO
        .fromAutoCloseable(ZIO.attempt(bigQueryTable.bigQueryService.writer(jobId, writeJobConfig)))
        .ensuring(waitForJob(jobId).ignore)
        .flatMap { writer =>
          ZIO.foreachDiscard(data) { row =>
            retry(writer.write(ByteBuffer.wrap(s"${row.asJson.noSpaces}\n".getBytes(StandardCharsets.UTF_8))))
          }
        }
    }
  }
}
